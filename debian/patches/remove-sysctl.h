Description: <sys/sysctl.h> is deprecated and seems to be unused here
Author: Thorsten Alteholz <debian@alteholz.de>
Index: npd6-1.1.0/includes.h
===================================================================
--- npd6-1.1.0.orig/includes.h	2020-08-06 19:48:08.625586340 +0000
+++ npd6-1.1.0/includes.h	2020-08-06 19:48:21.501646906 +0000
@@ -53,7 +53,6 @@
 #include <netinet/ip6.h>
 #include <netinet/icmp6.h>
 #include <arpa/inet.h>
-#include <sys/sysctl.h>
 #include <net/if.h>
 #include <getopt.h>
 #include <ifaddrs.h>
